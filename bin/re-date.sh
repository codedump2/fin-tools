#!/bin/bash

DIR=$1

[ -z "$DIR" ] && echo Usage: $0 dir && exit

declare -A months
months=(["Januar"]="01"
	["Februar"]="02"
	["März"]="03"
	["April"]="04"
	["Mai"]="05"
	["Juni"]="06"
	["Juli"]="07"
	["August"]="08"
	["September"]="09"
	["Oktober"]="10"
        ["November"]="11"
	["Dezember"]="12")

function copy()
{
    local __path="$1"
    local __date="$2"
    
    # copying, if date seems valid
    if date -d $__date > /dev/null 2>&1; then
	echo "$__path" "$(dirname $__path)/$__date.$(basename $__path)"
	mv "$__path" "$(dirname $__path)/$__date.$(basename $__path)"
    else
	echo Bad date: $ger_date / $yyyy $mon $dd / $__date
    fi    
}

for f in `ls $DIR/*pdf`; do
    file=`basename $f`

    # skip files that already begin in a YYYY-MM-DD date
    if date -d $(echo $file | cut -b1-10) > /dev/null 2>&1; then
 	echo Ignoring: $file
	continue
    fi

    filetext=$(pdf2txt "$DIR/$f")

    ger_date=$(echo -e "$filetext" | sed -n '/Rechnungsdatum/,+3p' | sed '1,+2d')
    if [ ! -z "$ger_date" ]; then
	# Amazon
	yyyy=$(echo $ger_date | cut -d\  -f3)
	mon=$(echo $ger_date | cut -d\  -f2)
	mm=${months[$mon]}
	dd=$(echo $ger_date | cut -d\  -f1)
	copy $f $yyyy-$mm-$dd
	continue
    fi

    ger_date=$(echo -e "$filetext" | sed -n '/Rechnungs-\/Lief.datum/,+2p' | sed '1d')
    if [ ! -z "$ger_date" ]; then
	# Conrad
	yyyy=$(echo $ger_date | cut -d. -f3)
	mon=$(echo $ger_date | cut -d. -f2)
	dd=$(echo $ger_date | cut -d. -f1)
	copy $f $yyyy-$mon-$dd
	continue
    fi

    #ger_date=$(echo -e "$filetext" | sed -n '/D-12557 Berlin/,+3p' | sed '1,+1d' | sed '2,~1d')
    #echo "<$ger_date>"
    #if [ ! -z "$ger_date" ]; then
#	# jacob elektronik
#	yyyy=$(echo $ger_date | cut -b7-10)
#	mon=$(echo $ger_date | cut -b4-5)
#	dd=$(echo $ger_date | cut -b1-2)
#	echo $yyyy // $mon // $dd
#	copy $f $yyyy-$mon-$dd
#	continue
#    fi
    
    echo Unknown: $file
done
