import csv
from datetime import datetime
import re

from beancount.core.amount import Amount
from beancount.core import data
from beancount.core.number import Decimal
from beancount.ingest import importer


BANKS = ('ING', 'ING-DiBa')

FIELDS = (
    'Buchung',
    'Valuta',
    'Auftraggeber/Empfänger',
    'Buchungstext',
    'Verwendungszweck',
    'Saldo',
    'Währung',
    'Betrag',
    'Währung',
)

META_KEYS = ('IBAN', 'Kontoname', 'Bank', 'Kunde', 'Zeitraum', 'Saldo')

PRE_HEADER = (
    'In der CSV-Datei finden Sie alle bereits gebuchten Umsätze. '
    'Die vorgemerkten Umsätze werden nicht aufgenommen, auch wenn sie in '
    'Ihrem Internetbanking angezeigt werden.'
)


class InvalidFormatError(Exception):
    pass


def _format_iban(iban):
    return re.sub(r'\s+', '', iban, flags=re.UNICODE)


def _format_number_de(value: str) -> Decimal:
    thousands_sep = '.'
    decimal_sep = ','

    return Decimal(value.replace(thousands_sep, '').replace(decimal_sep, '.'))


class ECImporter(importer.ImporterProtocol):
    def __init__(self, iban, account, user, file_encoding='ISO-8859-1'):
        self.iban = _format_iban(iban)
        self.account = account
        self.user = user
        self.file_encoding = file_encoding

        self._date_from = None
        self._date_to = None
        self._balance = None
        self._line_index = -1

    def file_account(self, _):
        return self.account

    def _read_header(self, fd, max_lines=20, entry_head="Buchung;Valuta;Auftraggeber"):
        '''
        Reads the header of a DiBa CSV file from an already open
        file object.
        Returns a header dictionary and a entry-head description list,
        e.g.:
        (head, entry) = ...
        head = { 'Umsatzanzeige': 'Erstellt am...',
                 'IBAN': '...', ...}
        entry = [ 'Buchung', 'Valuta', ... ]
        
        Raises a FormatError if the format does not match.

        To avoid long lock times with long invalid files, at most `maxlines` lines
        are read. If the abort condition is not found within those lines, a
        FormatError is raised.
        '''

        # skip through the header preample
        i = 0
        lines = [ fd.readline().strip() ]

        # quick n dirty test to exclude completely bad format
        if not lines[0].startswith("Umsatzanzeige"):
            raise RuntimeError("Not a DiBa CSV file")
        
        while True:
            line = fd.readline().strip()
            
            if i < max_lines:
                max_lines += 1
            else:
                raise RuntimeError("Header has too many lines, stopped at " % line)

            
            if line.startswith("Buchung;Valuta;Auftraggeber"):
                entry_head = []
                data = [i for i in  csv.reader([line], delimiter=';', quoting=csv.QUOTE_MINIMAL, quotechar='"')]

                # parse the entry header; problem is that entries are not unique, so we
                # need to make a hack to make them unique (add ...2, ...3 etc to non-unique
                # header entries)                
                #print (data)
                for i in data[0]:
                    #print ("Checking", i)
                    if i not in entry_head:
                        entry_head.append(i)
                    else:
                        start = 2
                        while True:
                            new_i = i+"%d"%start
                            if new_i not in entry_head:
                                entry_head.append(new_i)
                                break
                            start += 1
                
                break
            lines.append(line)

        # parse the header/preamble
        reader = csv.reader(lines, delimiter=';', quoting=csv.QUOTE_MINIMAL, quotechar='"')
        meta_head = { i[0]:i[1] for i in filter(lambda x: len(x) == 2, reader) }

        return meta_head, entry_head


    def identify(self, file_):        
        with open(file_.name, encoding=self.file_encoding) as fd:
            try:
                meta_head, entry_head = self._read_header(fd)
            except RuntimeError:
                return False

            if meta_head['IBAN'].replace(' ', '').lower() != self.iban.lower():
                #print ("Bad IBAN:", self.iban.lower(), "vs", meta_head['IBAN'].replace(' ', '').lower())
                return False
            else:
                #print ("IBAN match:", meta_head['IBAN'])
                pass

            if meta_head['Kunde'].lower() != self.user.lower():
                #print ("Bad User")
                return False

            if meta_head['Bank'] not in BANKS:
                #print ("Bad Bank")
                return False

        #print ("Match")
        #print (entry_head)
        return True
    

    def extract(self, file_, existing_entries=None):
        entries = []
        
        self._line_index = 0
        
        with open(file_.name, encoding=self.file_encoding) as fd:
            try:
                meta_head, entry_head = self._read_header(fd)
            except RuntimeError:
                return False
                
            # Data entries
            reader = csv.reader(fd, delimiter=';', quoting=csv.QUOTE_MINIMAL, quotechar='"')
            
            for i in reader:
                # we're going to access data by the names in the head
                csvt = { k:v for k, v in zip(entry_head, i) }

                meta = data.new_metadata(file_.name, self._line_index)
                amount = Amount(_format_number_de(csvt["Betrag"]), csvt["Währung2"])
                date = datetime.strptime(csvt["Buchung"], '%d.%m.%Y').date()

                description = '{} {}'.format(csvt["Verwendungszweck"], csvt["Buchungstext"]).strip()

                postings = [
                    data.Posting(self.account, amount, None, None, None, None)
                ]

                entries.append(
                    data.Transaction(
                        meta,
                        date,
                        self.FLAG,
                        csvt["Auftraggeber/Empfänger"],
                        description, #csvt["Verwendungszweck"],
                        data.EMPTY_SET,
                        data.EMPTY_SET,
                        postings,
                    )
                )

                self._line_index += 1

        return entries
                
