#!/usr/bin/python3

#
# TAFilter (Target Account Filter) is a particular type of importer.
# It actually doesn't import anything at all, it just takes an
# existing importer, wraps itself around it, and post-processes
# the generated Beancount transactions according to filter rules.
#

from beancount.ingest import importer
from beancount.core.data import Transaction, Posting, Amount
from beancount.core.number import Decimal, round_to

class TAFilter(importer.ImporterProtocol):
    '''
    TAFilter (Target Account Filter) is a particular type of importer.
    It actually doesn't import anything at all, it just takes an
    existing importer, wraps itself around it, and post-processes
    the generated Beancount transactions according to filter rules.

    The idea is that one would specify filter rules. Each rule consists
    of:
      - Responsibility condition (e.g. regex / globbing for Payee and Text)
      - List of target accounts and fractional payments

    Every transaction that matches a particular rule's Responsibility
    Condition is amended to pay out to Target Accounts corresponding
    to the list of fractional payment.
    This is useful for example to split an incoming payment from
    a customer in its VAT part and net income value, each going to
    different accounts.
    '''

    def __init__(self, realImporter, filterList, exclusive=False):
        '''
        realImporter: Importer object to proxy for
        filterList: List of the format [ {'predicate': p,
                                          'targets': { 'Account': fraction }, ... },
                                         { ... } ]
                    The filter list defines which Transactions
                    receive which Postings.
        '''
        self.realImporter = realImporter
        self.filterList = filterList
        self.exclusive = exclusive        

    def __str__(self):
        return "TargetAccountFilter."+self.realImporter.__str__()

    def file_account(self, file):
        return self.realImporter.file_account(file)

    def file_date(self, file):
        return self.realImporter.file_date(file)

    def file_name(self, file):
        return self.realImporter.file_name(file)

    def identify(self, file):
        return self.realImporter.identify(file)

    def name(self):
        return self.__str__()
    
    def extract(self, file, existing_entries=None):
        '''
        This is where the magic happens
        '''
        result = self.realImporter.extract(file, existing_entries)

        newResult = []

        ##print ("Filter list:", self.filterList)

        for entry in result:
            for filter in self.filterList:
                ##print ("Filter:", filter)
                if filter['predicate'](entry):
                    for account in filter['targets']:
                        amount  = None
                        frac    = filter['targets'][account]
                        if frac is not None:
                            nr = entry.postings[0].units.number * Decimal(frac)
                            amount = Amount(round_to(nr, Decimal("0.01")),
                                            entry.postings[0].units.currency)
                        entry.postings.append(Posting(account, amount, None, None, None, None))

                    # empty targets list means ignore transaction
                    if len(filter['targets']):
                        newResult.append(entry)
                    break ## a valid filter predicate stops further processing for current entry
                                    
        return newResult
