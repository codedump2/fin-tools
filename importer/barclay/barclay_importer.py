#!/usr/bin/python3

from beancount.ingest import importer
from beancount.ingest import cache
from pandas import read_excel
from dateutil.parser import parse as date_parse
from datetime import datetime

from beancount.core.data import Posting, Amount, \
    Transaction, Decimal, Currency, new_metadata, EMPTY_SET

from xlrd.biffh import XLRDError

import pytest

# Brief format description
#
# As of as of 2021-03-01, columns are undefined ("unnamed"), but
# row 13 holds a human-readable key along the lines of:
#
#  0: Referenznummer
#  1: Transaktionsdatum
#  2: Buchungsdatum
#  3: Betrag in EUR
#  4: Beschreibung
#  5: Typ
#  6: Status
#  7: Kartennummer
#  8: Originalbetrag
#  9: Mögliche Ratenkäufe
# 10: Land
# 11: Name des Karteninhabers
# 12: Kartennetzwerk
# 13: Kontaktlose Bezahlung
#
# This is probably different in other languages. To stay compatible,
# we're going to use numbers, not names (and hope that the list is
# the same in other countries).
# Note that sometimes titles change (Yay!)
#

class BarclayImporter(importer.ImporterProtocol):
    '''
    Importer for BarclayCard .xlsx extracts
    '''

    def __init__(self, ccIban='', ccAccount='', fileInto='',
                 currencyMap={ '€': 'EUR', '$': 'USD' },
                 statusFilter=['Berechnet', 'noch nicht abgerechnet', 'abgerechnet']):
        
        '''
        Either one of ccIban or ccAccount is needed, and the fileInto account.
        
        Parameters:
          - ccIban: IBAN of the CC account
          - ccAccount: Account number of the CC
          - fileInto: Where to put transactions
          - currMap: How to translate currencies of the "Betrag in EUR"
            field (e.g. { '€': 'EUR' })
          - statusFilter: only consider transactions that have "status"
            as one of the items in filter. `None` means consider everything.
        '''
        
        self.ccIban = ccIban
        self.ccAccount = ccAccount
        self.fileInto = fileInto
        self.data = None
        self.currencyMap = currencyMap
        self.statusFilter = statusFilter

    def _open_file(self, file):
        '''
        Opens file and initializes stuff (self.data* fields)
        '''
        try:
            # Pandas will read the data and name all columns "Unnamed: ..."
            self.data = read_excel(file.name, sheet_name=0)
            
        except XLRDError:
            # Parse error, mismatched file format -- that's
            # just a regular negative answer.
            print ("; Bad format:", file.name)
            return False
        
        except ValueError as e:
            print ("Pandas bailing out:", e)
            return False

        #print ("; Opening", file.name)
        
        # the i18n-ed column names
        self.col_names = [ self.data[i][11] for i in self.data ]

        #print ("; Coloumns", self.col_names)

        # header data
        self.metaKeys = [ k for k in self.data['Unnamed: 0'][2:10] ]
        self.metaData = { str(k):str(v) for k,v in zip(self.data['Unnamed: 0'][2:10], self.data['Unnamed: 1'][2:10]) }
        
        self.dataAccount    = self.metaData['Kontonummer']            # e.g. "2234575983"
        self.dataIban       = self.metaData['IBAN'].replace(' ', '')  # e.g. "DE28 2234 .... ...."
        self.dataCardType   = self.metaData['Kontoname']         # e.g. "Barclaycard Platinum Double"
        self.dataCredit     = self.metaData['Kreditrahmen']      # e.g. 3.500,00
        self.dataCredAvail  = self.metaData['Verfügungsrahmen']  # e.g. "3.873,00" 
        self.dataCardStatus = self.metaData['Status']            # e.g. "Aktiv"
        self.dataTimeSpec   = self.metaData['Stand']             # e.g. "13 Feb 21"


    def __str__(self):
        return "BarclaycardImporter/"+self.ccAccount if self.ccAccount is not None else self.ccIban

    def file_account(self, file):
        return self.fileInto

    def file_date(self, file):
        try:
            if self.data is None:
                self._open_file(file)
        except:
            return None

        #print ("; Time:", self.dataTimeSpec)
        
        # a useful time stamp transalte from barclay-months to parseable months and parse
        dateTuple = self.metaData['Stand'].split(' ')
        dateTuple[1] = { 'Jan': 'Jan',  'Feb': 'Feb',  'Mrz': 'Mar',  'Apr': 'Apr',
                         'Mai': 'May',  'Jun': 'Jun',  'Jul': 'Jul',  'Aug': 'Aug',
                         'Sep': 'Sep',  'Oct': 'Oct',  'Nov': 'Nov',  'Dez': 'Dec' }[dateTuple[1]]
        self.dataTimeStamp  = date_parse(' '.join(dateTuple))
        
        #print ("; Timestamp:", self.dataTimeStamp)
            
        return self.dataTimeStamp.date()

    def file_name(self, file):
        if self.data is None:
            self._open_file(file)
        return "barclay-cc-"+self.dataAccount+".xlsx"

    
    def identify(self, file):
        #print ("ID:", file.name)
        try:
            if self.data is None:
                self._open_file(file)

            known = ("barclay" in self.dataCardType.lower()) and \
                    (self.ccIban == self.dataIban or self.ccAccount == self.dataAccount)

            #print ("Card type matches:",
            #       "barclaycard" in self.dataCardType.lower())
            #print ("IBAN matches:", self.ccIban == self.dataIban)
            #print ("Account matches:", self.ccAccount == self.dataAccount)
            #print ("Known:", known, "from card type:", self.dataCardType)

            #print ("; Known format / account:", known)
            return known

        # we name the expcted errors in case of "regular" bad format
        # explicitly, so that the script becomes loud if an unexpected error
        # comes around:
        except AttributeError:   ## raised when dataCardType not available
            return False
        except:
            print ("; Data parse error:", file.name)
            raise
            return False

    def name(self):
        return self.__str__()

    
    def extract(self, file, existing_entries=None):
        '''
        This is where the magic happens
        '''
        
        result = []
        line_no = 12

        #print ("Extracting")
        
        for (tr_refnr, tr_date, bk_date, amnt, narr, owner, status) \
            in zip(self.data['Unnamed: 0'][line_no:],
                   self.data['Unnamed: 1'][line_no:], 
                   self.data['Unnamed: 2'][line_no:],
                   self.data['Unnamed: 3'][line_no:],
                   self.data['Unnamed: 4'][line_no:],
                   self.data['Unnamed: 11'][line_no:],
                   self.data['Unnamed: 6'][line_no:]):

            if self.statusFilter and status not in self.statusFilter:
                continue

            #print ("Amount:", amnt, "date:", tr_date)

            # german amount is "x.xxx,xx €", so we have to:
            #  - split away the Euro and convert it to EUR
            #  - replace decimal by comma by dot
            #  - remove thousands dot
            (amnt_nr, amnt_cur) = amnt.split(' ')
            amount = Amount(Decimal(amnt_nr.replace('.', '').replace(',', '.')),
                            Currency(self.currencyMap[amnt_cur]))
            
            date = datetime.strptime(tr_date, "%d.%m.%Y").date()

            metalist = {}
            
            if isinstance(owner, str):
                metalist['cardOwner'] = owner.title()

            metalist['barclayRefNr'] = tr_refnr

            meta = new_metadata(file.name, line_no, metalist)
                
            transaction = Transaction(meta,       # meta
                                      date,       # date
                                      "*",        # flag
                                      "",         # payee (n/a for barclaycards)
                                      narr,       # narration
                                      EMPTY_SET,  # tags
                                      EMPTY_SET,  # links
                                      [Posting(self.fileInto, amount, None, None, None, None)])
            
            # The tr_refnr is a unique transaction ID within the barclaycard system.
            # We've been saving that as a "barclay:..." tag. Now we search for it
            # in the existing entries list, and if we find it, we flag the current
            # entry as a duplicate. Beancount will (presumably) do its magic and
            # handle this according its settings.
            if existing_entries:
                for t in existing_entries:
                    if ref == t.meta.get('barclayRefNr'):
                        transaction.__duplicate__ = True
                        break
                                    
            result.append(transaction)

            line_no += 1

        return result


@pytest.fixture
def test_file():
    '''
    Returns a cache.FileMemo object of a test file
    '''

    from os import path

    testfile = path.join(path.dirname(__file__), "testdata", "example2.xlsx")
    
    return cache.get_file(testfile)
    

def test_importer(test_file):
    '''
    Testing Barclaycard importer
    '''

    imp = BarclayImporter('DE12345678901234567890', '1234567890')
    
    imp.file_name(test_file)
    entries = imp.extract(test_file)

    print("Entries:", len(entries))

    # the file has 13 valid entries, one not yet processed
    assert len(entries) > 0

    for t in entries:
        print (t.date, t.postings)

