FROM registry.fedoraproject.org/fedora-toolbox:36
ENV NAME=fin-fedora-toolbox VERSION=36

RUN \
	dnf install --assumeyes \
	    ledger hledger hledger-web aqbanking python3-pip git gcc python3-devel \
	    findutils python3-wheel which bc gawk sed git iproute nano nmap-ncat \
	    python3-pandas python3-openpyxl

RUN \
	cd /opt && \
	git clone --recurse-submodule https://github.com/beancount/beancount beancount-v2 && \
	cd beancount-v2 && \
	git checkout 2.3.3 && \
	git submodule update && \
	python3 setup.py install

RUN \
	pip3 install beancount-fava beancount-ing-diba beancount-dkb pinto pyledger

RUN \
	cd /opt && \
	git clone --recurse-submodule https://gitlab.com/codedump2/fin-tools

# Prevent "permission denied" when running container with --userns=keep-id
RUN touch /root/.bashrc && chmod 0666 /root/.bashrc && chmod a+x /root && \
    echo "fin-tools" > /etc/hostname

ENV PATH=$PATH:/opt/fin-tools/bin:/opt/beancount-v2/bin
ENV PYTHONPATH=$PYTHONPATH:/opt/fin-tools
ENV PS1='\u@finance \W \$ '

LABEL   com.github.containers.toolbox="true" \
        usage="This image is meant to be used with the toolbox command" \
	summary="Base image for creating Fedora toolbox containers"

# The scenario for this container is that the calling user
# logs in as himself into the container and works on files in his
# own host homedir. Have a dedicated unprivileged user, but
# unfortunately aqbank-cli is broken with regards to custom config
# directories. So, essentially, what we have to do is export the aqbanking
# confdir to /.aqbanking via the -v flag and uses --userns=keep-id to
# obtain a consistent identity for himself in both host and container.
# Home unfortunately will default to '/'.

#RUN	mkdir -p /home/worker/.aqbanking && ln -s /home/worker /home/florin && \
#	useradd -u 480 -d /home/worker -s /bin/bash worker && \
#	chown worker /home/worker -R
#USER worker
