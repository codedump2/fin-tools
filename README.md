# Command line accounting for the lazy

## The `fin-tools` collection
This is a finance and accounting containerized toolbox based mainly
on `beancount` and `aqbanking`. The main directory contains a
`Dockerfile` that builds the container. The bin directory contains
several helper scripts.

The idea is that a user downloads this and can go ahead and use
command-line accounting right away. This toolset contains only the
non-sensitive program part. Sensitive data like `aqbanking` setup
and secrets should be contained in a different repository.

## The sensitive `fin-data` repository

You should have a `fin-data` directory or repository somewhere, which
essentially should contain the following:

  - `aq-secrets`: A folder that should contain a file named `pins` which
     holds `aqbanking` secret pins for authenticating the HBCI connection.
	 It will be passed to `aqbaking-cli` via the command line option
	 `--pinfile=aq-secrets/pins`.
	 
	 The default location of this folder is `../fin-data/aq-secrets/`
	 with respect to the fin-tools base folder. If different, its full path
	 should be exported as `$FIN_SECRETS` when using fin-tools.
	 
  - `aq-conf`: A persistent, (initially empty) folder that will contain
    the `aqbanking` accounts configuration. It will be mounted inside
	the fin-tools container as the `~/.aqbanking` folder.
	
	The default location of this folder is `../fin-data/aq-conf/` relative
	to the fin-fools base folder. If different, its location must be
	exported as `$FIN_CONF`.
	
  - A data folder that will contain downloaded transtaction data (`aqbanking`
    CTX files) aswell as, at your option, other financital data (ledgers,
	invoices). By default, this is assumed to be `../fin-data/` relative
	to the fin-tools folder. If different, its location should be
	exported as `$FIN_DATA`.
	
Note that in the setup above (my setup), the data folder is the main folder,
with `aq-conf` and `aq-secrets` being subdirectories inside that folder. This
is the preferred way of doing it because then all the sensitive and
person-specific financial data is in "one basket", being separated from the
tooling, but not fragmented over too many repositories. However, one could
choose to have a different setup, e.g. involving three distinct directories
at locations not related to one another.
  

## The indivitual tools

The following tools are available from the host system:

  - `run`: Executes a command within the fin-tools container
  
  - `shell`: Executes a login bash shell within the container
  
The following commands are also available, but must be run
from within the conainer, either by opening a shell in the container
or by prepending them with `fin run`:

  - `retr`: Retrieves CTX data via `aqbanking-cli`.
  
  - ...lots of command line tools (e.g. `beancount`, `aqbaking-cli`,
    etc).

## Initial setup

First obtain the `fin-tools` and your personal `fin-data`:

```
   git clone https://gitlab.com/codedump2/fin-tools.git
   git clone <your-url>/fin-data.git
```

Alternatively, you can create your fin-data folder, if you don't
already have one:

```
   mkdir fin-data
   cd fin-data
   git init
   mkdir aq-conf
   mkdir aq-secrets
   echo "PIN_XXXXXXXX_YYYYYYYY=\"password\"" > aq-secrets/pins
```

Finally generate the image and test it:

```
   podman build -f fin-tools/Dockerfile -t finance
   fin-tools/bin/fin run beancount --version
```

Add a HBCI connection (quick 'n' dirty version, e.g. for ING-DiBa):

```
    aqhbci-tool4 adduser -N "Your Name" -u <ACCOUNTNR> -c <ACCOUNTNR> -b <BANKNR-BLZ> \
                         -s https://fints.ing-diba.de/fints/ -t pintan
    aqhbci-tool4 getsysid -u <UNIQUEID> ## from:aq-conf/settings6/users/...conf:uniqueId>
    aqhbci-tool4 getaccounts -u <UNIQUEID>
    aqhbci-tool4 listaccounts
```

You can create a simple shell script in your favorite `bin/` folder
to aid with command line:

```
    echo -e "#!/bin/bash \n\n fin-tools/bin/fin $@" >> $HOME/bin/fin
	chmod a+x $HOME/bin/fin
```

## Daily bookkeeping routine

Run the fin-tool of your choice:

```
   fin retr -i DExxxxxxx<yourIBANhere> -f 20210101 -t 20210131 \
            -o fin-data/ctx/<IBAN>/2020-0101.ctx
			
   fin run beancount ...
   
   fin shell
   
   ...
```
